package mojos;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;


@Mojo(name = "projectInfoMailSender")
public class ProjectInfoMailSenderMojo extends AbstractMojo {

    @Parameter(property = "projectInfoMailSender.username")
    private String username;

    @Parameter(property = "projectInfoMailSender.password")
    private String password;

    @Parameter(property = "projectInfoMailSender.name", defaultValue = "DefaultProject")
    private String projectName;


    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        MailSender mailSender = new MailSender(username, password);

        if (new File("C:/LectionEpam/mvn-project-info.txt").exists()) {
            mailSender.sendAttachment(projectName + ": installed successfully.",
                    "This is project info e-mail. Don't respond.", username, username,
                    "C:/LectionEpam/mvn-project-info.txt");
        } else {
            mailSender.sendMessage(projectName + ": installed successfully.",
                    "The report is missed.", username, username);

        }
    }
}
