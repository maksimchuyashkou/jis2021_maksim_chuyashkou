package mojos;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.FileOutputStream;
import java.io.IOException;


@Mojo(name = "projectInfoWriter")
public class ProjectInfoWriterMojo extends AbstractMojo {

    @Parameter(property = "projectInfoWriter.groupId", defaultValue = "")
    private String groupId;

    @Parameter(property = "projectInfoWriter.artifactId", defaultValue = "")
    private String artifactId;

    @Parameter(property = "projectInfoWriter.version", defaultValue = "")
    private String version;

    @Parameter(property = "projectInfoWriter.buildTimestamp", defaultValue = "")
    private String buildTimestamp;


    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        try {
            FileOutputStream fileOutputStream = new FileOutputStream("C:/LectionEpam/mvn-project-info.txt");
            String greetings = toString();
            fileOutputStream.write(greetings.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "groupId: " + groupId + '\n' +
                "artifactId: " + artifactId + '\n' +
                "version: " + version + '\n' +
                "buildTimestamp: " + buildTimestamp;
    }
}

