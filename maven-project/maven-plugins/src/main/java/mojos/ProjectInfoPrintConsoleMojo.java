package mojos;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;


@Mojo(name = "projectInfoPrintConsole")
public class ProjectInfoPrintConsoleMojo extends AbstractMojo {

    @Parameter(property = "projectInfoPrintConsole.groupId", defaultValue = "")
    private String groupId;

    @Parameter(property = "projectInfoPrintConsole.artifactId", defaultValue = "")
    private String artifactId;

    @Parameter(property = "projectInfoPrintConsole.version", defaultValue = "")
    private String version;

    @Parameter(property = "projectInfoPrintConsole.buildTimestamp", defaultValue = "")
    private String buildTimestamp;


    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info(toString());
    }

    @Override
    public String toString() {
        return "groupId: " + groupId + '\n' +
                "artifactId: " + artifactId + '\n' +
                "version: " + version + '\n' +
                "buildTimestamp: " + buildTimestamp;
    }

}
